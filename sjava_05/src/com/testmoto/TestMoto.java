package com.sjava.testmoto;

class TestMoto {
    public static void main(String[] args) {
        Moto moto1 = new Moto("Yamaha", "MT07", 689, 75, 6799 );
        Moto moto2 = 
            new Moto("Kawasaki", "Ninja 650", 649, 68, 7250 );
        
        moto1.comparaCv(moto2);
    }
}