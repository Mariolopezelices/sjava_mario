import java.util.Scanner;

class Palindromo {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        System.out.printf("Entra palabra: ");
        String palabra = keyboard.nextLine();

        palabra.toLowerCase();

        char[] charArray = palabra.toCharArray();
        //
        char ultimaletra = charArray[charArray.length - 1];

        charArray[charArray.length - 1] = Character.toUpperCase(ultimaletra);

        StringBuilder creador = new StringBuilder();

        for (int i = palabra.length(); i > 0; i--) {
            creador.append(charArray[i - 1]);
        }

        String palindromo = creador.toString();

        System.out.println(palindromo);
        System.out.println(capitaliza(palindromo));

    }

    public static String capitaliza(String palabra) {

        String parte1 = palabra.substring(0, 1);
        String parte2 = palabra.substring(1);

        return parte1.toUpperCase() + parte2.toLowerCase();

    }

}