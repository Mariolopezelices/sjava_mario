import org.omg.CORBA.DataOutputStream;

class InfoNums {
    public static void main(String[] args) {
        int cant = args.length;
        if (cant == 0) {
            System.out.print("Escriba numeros por favor.");
            return;
        }

        int dato = Integer.parseInt(args[0]);
        int total, mayor, menor;
        total = mayor = menor = dato;

        if (cant > 1) {
            for (int i = 1; i < cant; i++) {
                int num = Integer.parseInt(args[i]);
                total += num;
                if (mayor < num)
                    mayor = num;
                if (menor > num)
                    menor = num;
            }
        }
        if (cant == 0) {
            System.out.print("Escriba numeros por favor.");
        } else {
            int mediaA = total / cant;
            System.out.println("Cantidad de numeros introducidos:" + cant + ".");
            System.out.println("La suma total es: " + total + ".");
            System.out.println("La media Aritmetica: " + mediaA + ".");
            System.out.println("El numero mayor es: " + mayor + ".");
            System.out.println("El numero menor es: " + menor + ".");

        }
    }
}
