import java.util.Random;
import java.awt.SystemColor;
import java.util.Scanner;

class Adivina {
    public static void main(String[] args) {
        int num = 5; // tiene que estar entre 1-10.Para que no de error.
        int turnos = 0; // turnos inicio.
        int maxturnos = 3;// turnos maximos.

        Random random = new Random();
        int incognita = random.nextInt(10) + 1;

        Scanner keyboard = new Scanner(System.in);
        System.out.println("---Acierta el numero aleatorio:---");
        System.out.println("----Del 1-10 tienes 3 intentos----");
        System.out.println("----------------------------------");
        do {
            System.out.printf("Entra un num: ");
            try {
                // System.out.println(+incognita);
                num = keyboard.nextInt();
                turnos++;
                if (incognita != num) {
                    System.out.println("No es el correcto.");
                } else
                    System.out.println("Has acertado");

                if (maxturnos == turnos) {
                    System.out.println("El numero aleatorio era " + incognita + ". Has perdido.");
                }
                if (num <= 0 || num > 11) {
                    System.out.println("***Dato incorrecto: Introduzca del 1-10***");
                }

            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (num > 0 && num < 11 && maxturnos != turnos && incognita != num);
        keyboard.close();
    }
}
