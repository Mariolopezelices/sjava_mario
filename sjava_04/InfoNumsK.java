
import java.awt.SystemColor;
import java.util.Scanner;

class InfoNumsK {
    public static void main(String[] args) {
        int cant = 0;
        int total, mayor, menor;
        int num = 999;
        total = 0;
        mayor = 0;
        menor = 1000;

        Scanner keyboard = new Scanner(System.in);
        
        do {
            System.out.printf("Entra un num: ");
            try {
                num = keyboard.nextInt();
                if (mayor < num) {
                    mayor = num;
                }
                if (0 == num) { // Evita que se ponga 0 como menor valor.
                    cant--; // Restar 1 para no contar el 0.
                    System.out.println("Final");
                } else if (menor > num) {
                    menor = num;
                }
                total += num;
                cant++;
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
            }
        } while (num > 0);

        float mediaA = (float) total / cant;

        System.out.println("La suma es " + total + ".");
        System.out.println("La cantidad de numeros es: " + cant + ".");
        System.out.println("La media aritmetica es: " + mediaA + ".");
        System.out.println("El numero mayor introducido es : " + mayor + ".");
        System.out.println("El numero menor introducido es : " + menor + ".");

        keyboard.close();
    }

}
