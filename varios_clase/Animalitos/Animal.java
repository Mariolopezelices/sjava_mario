
public class Animal {
    public static void main(String[] args) {
        Animal a = new Perro();
        a.duerme();
        ((Perro)a).ladra();

    }

    private String color = "";

    public void duerme() {
        System.out.printf("A dormir!");
    }

    public void come() {
        System.out.println("Acomer!");
    }

    public int getNumeroPatas() {
        return 0;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
