
class Hombre extends Persona {

    @Override
    public String getNombre (){
        return "el Sr, "+this.nombre;
    }

    @Override
    public int getEdad(){
        return this.edad;
    }

    public Hombre (String nombre, int edad){
        super(nombre, edad);
    }



}