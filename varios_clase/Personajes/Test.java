
public class Test{
    public static void main(String[] args) {
        Persona m = new Mujer("Ana", 73);
        Persona h = new Hombre("Mario", 50);

        System.out.println(m.presentate());
        System.out.println(h.presentate());
    }
}