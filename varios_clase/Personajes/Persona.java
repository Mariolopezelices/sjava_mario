
class Persona {

    protected String nombre;
    protected int edad;

    Persona(String nombre, int edad){
        this.nombre= nombre;
        this.edad = edad;
    }

    public String getNombre() {
        return this.nombre;
    }

    public int getEdad() {
        return this.edad;
    }

    public String presentate(){
        return "Hola, soy " +this.getNombre()+" y tengo "+this.getEdad()+"años.";
    }
}