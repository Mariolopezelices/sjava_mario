
class Mujer extends Persona {
    
    @Override
    public String getNombre (){
        return "la Sra, "+this.nombre;
    }

    @Override
    public int getEdad(){
        return (this.edad>45) ? this.edad-5 : this.edad;
    }


    public Mujer (String nombre, int edad){
        super(nombre, edad);
    }

}