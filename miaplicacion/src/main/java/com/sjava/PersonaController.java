package com.sjava;

import java.util.ArrayList;
import java.util.Scanner;

public class PersonaController {
    private static ArrayList<Persona> contactos = new ArrayList<Persona>();
    private static int contador = 0;
    private static Scanner keyboard = new Scanner(System.in);
    private static int id = 2;
    
    public static void muestraContactos() {
        for (Persona p : contactos) {
            System.out.println(p);
        }
    }

    public static void muestraContactoId() {
        for (Persona p : contactos) {
            try {
                id = keyboard.nextInt();
                if (p.getId() == id) {
                    System.out.println(p);
            }} catch (Exception e) {
                System.out.println("***ID incorrecta***");
                keyboard.next();
            }
        }
        while (id > 0);
        System.out.println("Id Inexistente");
        keyboard.close();
    }
    
    public static void borraContactoId(int id) {
        for (Persona p : contactos) {
            if (p.getId() == id) {
                contactos.remove(p);
                return;
            }
        }
    }

    // monstrara contactos en pantalla.
    public static ArrayList<Persona> getContactos() {
        return contactos;
    }

    public static void nuevoContacto(Persona pers) {
        contador++;
        pers.setId(contador);
        contactos.add(pers);
    }

    // no se usa
    public static int numContactos() {
        return contactos.size();
    }
}
