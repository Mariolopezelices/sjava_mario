package com.sjava;

public class Persona {

    private String nombre;
    private String email;
    private int anyo;
    private int id;

    public Persona(String nombre, int anyo, String email) {
        this.nombre = nombre;
        this.email = email;
        this.anyo = anyo;
        PersonaController.nuevoContacto(this);
    }

    public String getNombre(){
        return this.nombre;
    }

    public String getEmail(){
        return this.email;
    }
  
    public int getAnyo(){
        return this.anyo;
    }

    public void setId(int id){
        this.id=id;
    }

    public int getId(){
        return this.id;
    }
    

    @Override
    public String toString() {
        return String.format("[%d] : %s (%s) %d", this.id, this.nombre, this.email, this.anyo);
    }
}