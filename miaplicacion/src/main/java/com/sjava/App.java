package com.sjava;


/**
 * Creo los objetos y demas.
 *
 */
public class App 
{     
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        //SJAVA 11---------------
        new Persona("ana", 1995, "ana@gmail.com");
        new Persona("claudia", 1999, "claudia@gmail.com");
        new Persona("Mario", 1993, "mario@gmail.com");
        new Persona("clau", 2009, "clsdffsfa@gmail.com");
        new Persona("Marcelino", 1987, "adadsfsio@gmail.com");
        PersonaController.muestraContactos();
        System.out.println("---------------------");
        PersonaController.muestraContactoId();
        System.out.println("---------------------");
        PersonaController.borraContactoId(3 & 2);
        System.out.println("------Borro contacto ID:3 y 2 ----------");
        PersonaController.muestraContactos();
    }
}
