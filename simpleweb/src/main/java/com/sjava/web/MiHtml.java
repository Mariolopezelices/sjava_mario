package com.sjava.web;

public class MiHtml {

    public static String stringsUl(String[] elementos) {

        StringBuilder sb = new StringBuilder();

        sb.append("<ul>");
        for (String s : elementos) {
            sb.append("<li>"+s+"</li>");
        }
        sb.append("</ul>");

        return sb.toString();
    }
}
