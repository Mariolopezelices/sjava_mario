class Person2 {
    String nombre;
    int edad;

    public Person2(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public void comparaEdad(Person2 otra) {
        if (this.edad > otra.edad) {
            System.out.printf("%s es mayor que %s.", this.nombre, otra.nombre);
        } else {
            System.out.printf("%s es mayor que %s.", otra.nombre, this.nombre);
        }
    }
}